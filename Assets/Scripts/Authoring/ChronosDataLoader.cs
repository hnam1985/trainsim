using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Authoring
{
	public class ChronosDataLoader : MonoBehaviour
	{
		public TextAsset ChronosJson;
		public GameObject VisualContainer;
		public float RailThickness = 0.1f;
		public Material RailMaterial;

		public GameObject TrainPrefab;
		public int TrainSpeed = 20;
		public int TrainSpeedRange = 5;		
		public int TrainSpeedVariation = 10;
		public int TrainSpeedVariationRange = 10;

		//Jasons transform for the moment, should be EPSG 3857 Mapping projection
		private Vector3 LonLatToPoint(float lon, float lat)
		{
			double cx = (-1.5550 + -1.5400) / 2; //lon
			double cy = (53.7925 + 53.7955) / 2; //lat
			return new Vector3((float) ((lon - cx) * 500), 0, (float) ((lat - cy) * 500));
		}

		public void Generate()
		{
			var chronos = JsonUtility.FromJson<ChronosData>(ChronosJson.text);
			var map = new Dictionary<long, SignalAuthoring>();

			foreach (var detail in chronos.geographyDetails)
			{
				foreach (var node in detail.nodes)
				{
					if (map.ContainsKey(node.node_osm_id)) continue;
					var pos = LonLatToPoint(node.lon, node.lat);

					var go = new GameObject("node " + node.node_osm_id);
					var signalAuthoring = go.AddComponent<SignalAuthoring>();
					signalAuthoring.transform.SetParent(this.transform);
					signalAuthoring.transform.position = pos;

					map.Add(node.node_osm_id, signalAuthoring);
				}
			}

			foreach (var detail in chronos.geographyDetails)
			{
				long node0 = detail.nodes[0].node_osm_id;
				var train = new GameObject("spawner " + node0);
				train.transform.SetParent(transform);
				var trainAu = train.AddComponent<IntervalSpawnerAuthoring>();
				trainAu.Interval = Random.Range(60, 90);
				trainAu.Prefab = TrainPrefab;
				trainAu.Location = map[node0];
				train.transform.position = map[node0].transform.position;
				trainAu.Speed = Random.Range(TrainSpeed, TrainSpeed + TrainSpeedRange);
				trainAu.SpeedVariation = Random.Range(TrainSpeedVariation, TrainSpeedVariation + TrainSpeedVariationRange);

				for (int i = 0; i < detail.nodes.Length - 2; i++)
				{
					var node = detail.nodes[i];
					var nextNode = detail.nodes[i + 1];
					map[node.node_osm_id].ConnectTo = map[nextNode.node_osm_id];
					CreateLine(map[node.node_osm_id].transform.position,
						map[nextNode.node_osm_id].transform.position);
				}
			}
		}

		private void CreateLine(Vector3 from, Vector3 to)
		{
			var go = new GameObject();
			GameObjectUtility.SetStaticEditorFlags(go, StaticEditorFlags.BatchingStatic);
			go.transform.SetParent(VisualContainer.transform);
			var line = go.AddComponent<LineRenderer>();
			line.positionCount = 2;
			line.SetPosition(0, from);
			line.SetPosition(1, to);
			line.widthMultiplier = RailThickness;
			line.material = RailMaterial;
		}

		public void Clear()
		{
			for (int i = transform.childCount; i-- > 0;)
			{
				DestroyImmediate(transform.GetChild(0).gameObject);
			}

			for (int i = VisualContainer.transform.childCount; i-- > 0;)
			{
				DestroyImmediate(VisualContainer.transform.GetChild(0).gameObject);
			}
		}
	}
}