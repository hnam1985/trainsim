using Simulation;
using Simulation.Components;
using Unity.Entities;
using UnityEngine;

namespace Authoring
{
	public class TrainAuthoring: MonoBehaviour, IConvertGameObjectToEntity
	{
		public SignalAuthoring Location;
		public int Speed = 300;
		public int SpeedVariation = 200;

		public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
		{
			dstManager.AddComponentData(entity, new Train
			{
				SpeedMin = Speed,
				SpeedVariation = SpeedVariation,
			});
			var signalEntity = Location == null ? Entity.Null : conversionSystem.GetPrimaryEntity(Location.gameObject);
			dstManager.AddComponentData(entity, new TrainCord
			{
				Cord = 0,
				Signal = signalEntity,
			});
//			dstManager.AddComponentData(signalEntity, new SignalData
//			{
//				Train = entity,
//				Timestamp = Constants.GetCurrentTime(),
//			});
		}
	}
}