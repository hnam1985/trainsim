using System.Collections.Generic;
using Simulation;
using Simulation.Components;
using Unity.Entities;
using UnityEngine;

namespace Authoring
{
	public class SpawnerAuthoring : MonoBehaviour, IConvertGameObjectToEntity, IDeclareReferencedPrefabs
	{
		public GameObject Prefab;
		public int Count = 1;
		public Vector3 Offset;
		
		public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
		{
			dstManager.AddComponentData(entity, new Spawner
			{
				Prefab = conversionSystem.GetPrimaryEntity(Prefab),
				Count = Count,
				Offset = Offset,
			});
		}

		public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
		{
			referencedPrefabs.Add(Prefab);
		}
	}
}