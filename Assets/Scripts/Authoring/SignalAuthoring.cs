using Simulation;
using Simulation.Components;
using Unity.Entities;
using UnityEngine;

namespace Authoring
{
	public class SignalAuthoring: MonoBehaviour, IConvertGameObjectToEntity
	{
		public SignalAuthoring ConnectTo;
		public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
		{
			dstManager.AddComponentData(entity, new Signal
			{
				NextSignal = ConnectTo == null ? Entity.Null : conversionSystem.GetPrimaryEntity(ConnectTo.gameObject),
			});

			if (ConnectTo != null)
			{
				dstManager.AddComponentData(entity, new BerthLength
				{
					Length = (transform.position - ConnectTo.transform.position).magnitude.ToInt(),
				});
			}
		}

//		private void OnDrawGizmos()
//		{
//			if (ConnectTo != null)
//			{
//				Gizmos.color = Color.red;
//				Gizmos.DrawLine(transform.position, ConnectTo.transform.position);
//			}
//			Gizmos.color = Color.blue;
//			Gizmos.DrawCube(transform.position, Vector3.one);
//		}
	}
}