using System;

namespace Authoring
{
	[Serializable]
	public class ChronosData
	{
		public GeographyDetail[] geographyDetails;
	}

	[Serializable]
	public class GeographyDetail
	{
		public Node[] nodes;
	}

	[Serializable]
	public class Node
	{
		public float lat;
		public float lon;
		public long node_osm_id;
	}
}