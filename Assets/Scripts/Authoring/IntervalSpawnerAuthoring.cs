using System.Collections.Generic;
using Simulation;
using Simulation.Components;
using Unity.Entities;
using UnityEngine;

namespace Authoring
{
	public class IntervalSpawnerAuthoring : MonoBehaviour, IConvertGameObjectToEntity, IDeclareReferencedPrefabs
	{
		public GameObject Prefab;
		public int Interval = 60; //in frame
		
		public int Speed;
		public int SpeedVariation;
		public SignalAuthoring Location;

		public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
		{
			dstManager.AddComponentData(entity, new IntervalSpawner
			{
				Prefab = conversionSystem.GetPrimaryEntity(Prefab),
				Interval = Interval,
				
				Speed = Speed,
				SpeedVariation = SpeedVariation,
				Location = conversionSystem.GetPrimaryEntity(Location.gameObject),
			});
		}

		public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
		{
			referencedPrefabs.Add(Prefab);
		}
	}
}