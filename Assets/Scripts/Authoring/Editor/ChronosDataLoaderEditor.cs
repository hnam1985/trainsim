using UnityEditor;
using UnityEngine;

namespace Authoring.Editor
{
	[CustomEditor(typeof(ChronosDataLoader))]
	public class ChronosDataLoaderEditor:UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			var loader = (ChronosDataLoader) target;
			if (GUILayout.Button("Generate"))
			{
				loader.Generate();
			}
			if (GUILayout.Button("Clear"))
			{
				loader.Clear();
			}
		}
	}
}