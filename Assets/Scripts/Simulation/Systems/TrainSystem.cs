using Simulation.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;

namespace Simulation.Systems
{
	[UpdateInGroup(typeof(SimulationSystemGroup))]
	public class TrainSystem : BaseSystem<EndSimulationEntityCommandBufferSystem>
	{
		[ExcludeComponent(typeof(TrainSpeed))]
		private struct TrainSpeedCompute: IJobForEachWithEntity<Train>
		{
			public EntityCommandBuffer.Concurrent CommandBuffer;
			public void Execute(Entity entity, int index, [ReadOnly] ref Train train)
			{
				CommandBuffer.AddComponent(index, entity, new TrainSpeed
				{
					Speed = train.SpeedMin + (entity.Index % train.SpeedVariation),
				});
			}
		}
		[ExcludeComponent(typeof(TrainCordData))]
		private struct TrainDataCompute: IJobForEachWithEntity<TrainCord>
		{
			public EntityCommandBuffer.Concurrent CommandBuffer;
			[ReadOnly] public ComponentDataFromEntity<Signal> Signals;
			[ReadOnly] public ComponentDataFromEntity<Translation> Translations;
			[ReadOnly] public ComponentDataFromEntity<BerthLength> BerthLengths;
			public void Execute(Entity entity, int index, 
				[ReadOnly] ref TrainCord trainCord)
			{
				var signalEntity = trainCord.Signal;
				var signal = Signals[signalEntity];
				if (signal.NextSignal != Entity.Null)
				{
					var startPos = Translations[signalEntity].Value;
					var endPos = Translations[signal.NextSignal].Value;
					CommandBuffer.AddComponent(index, entity, new TrainCordData
					{
						StartPos = startPos,
						EndPos = endPos,
						Length = BerthLengths[signalEntity].Length,
					});
				}
				else
				{
					CommandBuffer.AddComponent(index, entity, new ToBeDestroyed{});
				}
			}
		}

		[BurstCompile]
		private struct TrainMoveJob : IJobForEach<TrainSpeed, TrainCord>
		{
			public void Execute([ReadOnly] ref TrainSpeed train, ref TrainCord trainCord)
			{
				trainCord.Cord += train.Speed;
			}
		}

		private struct NextSignalJob : IJobForEachWithEntity<TrainCord, TrainCordData>
		{
			public EntityCommandBuffer.Concurrent CommandBuffer;
			[ReadOnly] public ComponentDataFromEntity<Signal> Signals;

			public void Execute(Entity entity, int index, ref TrainCord cord, ref TrainCordData cordData)
			{
				if (cord.Cord >= cordData.Length)
				{
					CommandBuffer.RemoveComponent<TrainCordData>(index, entity);
					cord.Cord = 0;
					cord.Signal = Signals[cord.Signal].NextSignal;
				}
			}
		}

		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			var commandBuffer = bufferSystem.CreateCommandBuffer().ToConcurrent();
			var signals = GetComponentDataFromEntity<Signal>();
			
			var trainSpeedJob = new TrainSpeedCompute
			{
				CommandBuffer = commandBuffer,
			}.Schedule(this, inputDeps);
			bufferSystem.AddJobHandleForProducer(trainSpeedJob);
			
			var trainDataJob = new TrainDataCompute
			{
				CommandBuffer = commandBuffer,
				Signals = signals,
				Translations = GetComponentDataFromEntity<Translation>(),
				BerthLengths = GetComponentDataFromEntity<BerthLength>(),
			}.Schedule(this, trainSpeedJob);
			bufferSystem.AddJobHandleForProducer(trainDataJob);
			
			var nextSignalJob = new NextSignalJob
			{
				CommandBuffer = commandBuffer,
				Signals = signals,
			}.Schedule(this, trainDataJob);
			bufferSystem.AddJobHandleForProducer(nextSignalJob);
			
			var trainMove = new TrainMoveJob().Schedule(this, nextSignalJob);
			
			return trainMove;
		}
	}
}