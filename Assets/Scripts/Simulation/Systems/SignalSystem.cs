//using Unity.Burst;
//using Unity.Collections;
//using Unity.Entities;
//using Unity.Jobs;
//using Unity.Mathematics;
//using Unity.Transforms;
//
//namespace Simulation
//{
//	public class SignalSystem : BaseSystem
//	{
//		[BurstCompile]
//		[ExcludeComponent(typeof(BerthDone))]
//		private struct BerthLengthJob : IJobForEachWithEntity<Signal>
//		{
//			public EntityCommandBuffer.Concurrent CommandBuffer;
//			[ReadOnly] public ComponentDataFromEntity<Translation> Translations;
//			public void Execute(Entity entity, int index, [ReadOnly] ref Signal signal)
//			{
//				if (signal.NextSignal != Entity.Null)
//				{
//					CommandBuffer.AddComponent(index, entity, new BerthLength
//					{
//						Length = math.length(Translations[entity].Value - Translations[signal.NextSignal].Value).ToInt(),
//					});
//				}
//				CommandBuffer.AddComponent(index, entity, new BerthDone{});
//			}
//		}
//
//		protected override JobHandle OnUpdate(JobHandle inputDeps)
//		{
//			var commandBuffer = bufferSystem.CreateCommandBuffer().ToConcurrent();
//			var lengthJob = new BerthLengthJob
//			{
//				CommandBuffer = commandBuffer,
//				Translations = GetComponentDataFromEntity<Translation>(),
//			}.Schedule(this, inputDeps);
//			bufferSystem.AddJobHandleForProducer(lengthJob);
//			return lengthJob;
//		}
//	}
//}