using Simulation.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;

namespace Simulation.Systems
{
	[UpdateInGroup(typeof(SimulationSystemGroup))]
	public class IntervalSpawnerSystem : BaseSystem<BeginSimulationEntityCommandBufferSystem>
	{
		[BurstCompile]
		private struct ElapseJob : IJobForEach<IntervalData>
		{
			public void Execute(ref IntervalData interval)
			{
				interval.Elapse--;
			}
		}

//		[BurstCompile] //CommandBuffer.Instantiate is not burst compatible
		private struct SpawnJob : IJobForEachWithEntity<IntervalSpawner, IntervalData>
		{
			public EntityCommandBuffer.Concurrent CommandBuffer;
			public void Execute(Entity entity, int index, [ReadOnly] ref IntervalSpawner spawner, ref IntervalData interval)
			{
				if (interval.Elapse == 0)
				{
					interval.Elapse = spawner.Interval;
					var train = CommandBuffer.Instantiate(index, spawner.Prefab);
					CommandBuffer.SetComponent(index, train, new Train
					{
						SpeedMin = spawner.Speed,
						SpeedVariation = spawner.SpeedVariation,
					});
					CommandBuffer.SetComponent(index, train, new TrainCord
					{
						Cord = 0,
						Signal = spawner.Location,
					});
				}
			}
		}
		
		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			var commandBuffer = bufferSystem.CreateCommandBuffer().ToConcurrent();
			var spawnJob = new SpawnJob
			{
				CommandBuffer = commandBuffer,
			}.Schedule(this, inputDeps);
			bufferSystem.AddJobHandleForProducer(spawnJob);
			
			var elapseJob = new ElapseJob{}.Schedule(this, spawnJob);

			return elapseJob;
		}
	}
}