using Simulation.Components;
using Unity.Entities;

namespace Simulation.Systems
{
	[UpdateInGroup(typeof(SimulationSystemGroup))]
	[UpdateBefore(typeof(TrainSystem))]
	public class CleanupSystem : ComponentSystem
	{
		private EntityQuery _toBeDestroyeds;
		protected override void OnCreate()
		{
			base.OnCreate();
			_toBeDestroyeds = EntityManager.CreateEntityQuery(typeof(ToBeDestroyed));
		}

		protected override void OnUpdate()
		{
			EntityManager.DestroyEntity(_toBeDestroyeds);
		}
	}
}