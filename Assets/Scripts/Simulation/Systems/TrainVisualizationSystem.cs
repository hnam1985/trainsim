using Simulation.Components;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

namespace Simulation.Systems
{
	[UpdateInGroup(typeof(PresentationSystemGroup))]
	public class TrainVisualizationSystem : JobComponentSystem
	{
		[BurstCompile]
		private struct VisualizeJob : IJobForEach<TrainCord, TrainCordData, Translation>
		{
			public void Execute([ReadOnly] ref TrainCord cord, [ReadOnly] ref TrainCordData cordData,
				ref Translation trans)
			{
				trans.Value = math.lerp(cordData.StartPos, cordData.EndPos, (float) cord.Cord / cordData.Length);
			}
		}

		protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			return new VisualizeJob().Schedule(this, inputDeps);
		}
	}
}