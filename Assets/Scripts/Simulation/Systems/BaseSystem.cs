using Unity.Entities;

namespace Simulation.Systems
{
	public abstract class BaseSystem<T> : JobComponentSystem where T:EntityCommandBufferSystem
	{
		protected EntityCommandBufferSystem bufferSystem;
		
		protected override void OnCreate()
		{
			base.OnCreate();
			bufferSystem = World.GetOrCreateSystem<T>();
		}
	}
}