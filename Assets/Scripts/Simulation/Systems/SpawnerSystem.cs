using Simulation.Components;
using Unity.Entities;
using Unity.Transforms;

namespace Simulation.Systems
{
	public class SpawnerSystem : ComponentSystem
	{
		protected override void OnUpdate()
		{
			Entities.ForEach((Entity entity, ref Spawner spawner) =>
			{
				for (int i = 0; i < spawner.Count; i++)
				{
					var newEntity = PostUpdateCommands.Instantiate(spawner.Prefab);
					PostUpdateCommands.SetComponent(newEntity, new Translation
					{
						Value = spawner.Offset * i,
					});
				}
				PostUpdateCommands.DestroyEntity(entity);
			});
			
			Entities.WithNone<IntervalData>().ForEach((Entity entity, ref IntervalSpawner spawner) =>
			{
				PostUpdateCommands.AddComponent(entity, new IntervalData
				{
					Elapse = spawner.Interval,
				});
			});
		}
	}
}