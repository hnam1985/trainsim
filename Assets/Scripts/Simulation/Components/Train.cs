using Unity.Entities;

namespace Simulation.Components
{
	public struct Train : IComponentData
	{
		public int SpeedMin; //cord increase per frame
		public int SpeedVariation; //cord increase per frame
	}
}