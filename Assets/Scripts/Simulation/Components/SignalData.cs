using Unity.Entities;

namespace Simulation.Components
{
	public struct SignalData : IComponentData
	{
		public Entity Train; //Entity.Null means no train!
		public int Timestamp;
	}
}