using Unity.Entities;

namespace Simulation.Components
{
	public struct BerthLength : IComponentData
	{
		public int Length;
	}
}