using Unity.Entities;
using Unity.Mathematics;

namespace Simulation.Components
{
	public struct Spawner : IComponentData
	{
		public Entity Prefab;
		public int Count;
		public float3 Offset;
	}
}