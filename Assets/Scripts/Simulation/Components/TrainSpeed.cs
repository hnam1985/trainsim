using Unity.Entities;

namespace Simulation.Components
{
	public struct TrainSpeed : IComponentData
	{
		public int Speed;
	}
}