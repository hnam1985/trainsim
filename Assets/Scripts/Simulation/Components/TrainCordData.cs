using Unity.Entities;
using Unity.Mathematics;

namespace Simulation.Components
{
	public struct TrainCordData : IComponentData
	{
		public float3 StartPos;
		public float3 EndPos;
		public int Length;
	}
}