using Unity.Entities;

namespace Simulation.Components
{
	public struct IntervalSpawner : IComponentData
	{
		public Entity Prefab;
		public int Interval;

		public int Speed;
		public int SpeedVariation;
		public Entity Location;
	}
}