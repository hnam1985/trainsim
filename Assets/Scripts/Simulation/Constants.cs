using Unity.Mathematics;
using UnityEngine;

namespace Simulation
{
	public static class Constants
	{
		private const int IntPrecision = 1000;
		
		public static int ToInt(this float val)
		{
			return (int)math.round(val * IntPrecision);
		}

		public static int GetCurrentTime()
		{
			return Time.time.ToInt();
		}
	}
}